<?php

namespace app\models;

use yii\base\Model;

class RegistrationForm extends Model
{
    public $login;
    public $password;
    public $confirmPassword;

    public function rules()  {
           return [
            ['login', 'filter', 'filter' => 'trim'],  
            ['login', 'required'],
            ['login', 'string', 'min' => 2, 'max' => 20],

            ['password', 'filter', 'filter' => 'trim'],
            ['password', 'required'],
            ['password', 'string', 'min' => 5, 'max' => 20],
     
            ['confirmPassword', 'filter', 'filter' => 'trim'],
            ['confirmPassword', 'required'],
            ['confirmPassword', 'compare', 'compareAttribute' => 'password']
               
        ];
    }
}
