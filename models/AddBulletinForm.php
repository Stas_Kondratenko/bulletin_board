<?php
namespace app\models;

use yii\base\Model;

class AddBulletinForm extends Model
{
    public $title;
    public $body;
    public $price;
    public $title_image;
    
    public function rules()  {
           return [  
            ['title', 'filter', 'filter' => 'trim'],  
            ['title', 'required'],
            ['title', 'string', 'min' => 5, 'max' => 40],

            ['body', 'filter', 'filter' => 'trim'],
            ['body', 'string', 'min' => 5, 'max' => 10000],
     
            ['price', 'filter', 'filter' => 'trim'],
            ['price', 'required'],
            ['price', 'integer', 'max' => 100000],
               
            ['title_image', 'image', 'extensions' => ['png', 'jpg', 'gif'], 
              'minWidth' => 100, 'maxWidth' => 1920,
              'minHeight' => 100, 'maxHeight' => 1080,]
        ];
    }
}