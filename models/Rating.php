<?php

namespace app\models;
use yii\db\ActiveRecord;

class Rating extends ActiveRecord 
{
    private $user_id;
    private $author_id;
    private $rating;
     
    public static function tableName(){
        return 'rating';
    }
}  