<?php

namespace app\models;
use yii\db\ActiveRecord;
use app\models\Bulletin;
use app\models\Comment;

class Profile extends ActiveRecord
{
    private $user_id;
    private $name;
    private $email;
    private $phone;
    private $skype;
    private $location;
    private $photo;
    
    public static function tableName(){
        return 'profile';
    }
    
    public function getComments(){
        return $this->hasMany(Comment::className(), ['user_id' => 'user_id'])->orderBy(['date' => SORT_DESC]);
    }
     public function getBulletins(){
        return $this->hasMany(Bulletin::className(), ['user_id' => 'user_id']);
    }
}  