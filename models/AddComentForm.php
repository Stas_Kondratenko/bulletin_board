<?php
namespace app\models;

use yii\base\Model;

class AddComentForm extends Model
{
    public $body;
    public $user_id;
    
    public function rules()  {
           return [  
            ['body', 'filter', 'filter' => 'trim'],  
            ['body', 'required'],
            ['body', 'string', 'min' => 5, 'max' => 255],

            ['user_id', 'filter', 'filter' => 'trim'],
        ];
    }
}