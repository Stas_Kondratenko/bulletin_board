<?php

namespace app\models;
use yii\db\ActiveRecord;

class Comment extends ActiveRecord 
{
    private $user_id;
    private $body;
    private $author_id;
    private $date;
     
    public static function tableName(){
        return 'comment';
    }
}  