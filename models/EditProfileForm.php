<?php
namespace app\models;

use yii\base\Model;

class EditProfileForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $skype;
    public $location;
    public $photo;
    
    public function rules()  {
           return [  
            ['name', 'filter', 'filter' => 'trim'], 
            ['name', 'string', 'max' => 40],
               
            ['email', 'filter', 'filter' => 'trim'], 
            ['email', 'email'],

            ['phone', 'filter', 'filter' => 'trim'],
            ['phone', 'integer'],
 
            ['skype', 'filter', 'filter' => 'trim'], 
            ['skype', 'string', 'max' => 40],
               
            ['location', 'filter', 'filter' => 'trim'], 
            ['location', 'string', 'max' => 40],
               
            ['photo', 'image', 'extensions' => ['png', 'jpg', 'gif'], 
              'minWidth' => 100, 'maxWidth' => 1920,
              'minHeight' => 100, 'maxHeight' => 1080,]
        ];
    }
}
