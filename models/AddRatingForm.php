<?php
namespace app\models;

use yii\base\Model;

class AddRatingForm extends Model
{
    public $rating;
    public $user_id;
    
    public function rules()  {
           return [  
            ['rating', 'filter', 'filter' => 'trim'],
            ['rating', 'required'],
            ['rating', 'integer','min' => 1, 'max' => 5],
               
            ['user_id', 'filter', 'filter' => 'trim'],
        ];
    }
}