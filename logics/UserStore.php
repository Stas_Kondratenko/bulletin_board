<?php
namespace app\logics;

use yii\web\IdentityInterface;
use yii\db\ActiveRecord;

class UserStore extends ActiveRecord implements IdentityInterface
{
    public static function tableName() {
        return 'user';
    }  
    
    public static function findByloginAndPassword($login, $password)   {
        return static::findOne([
            'login' => $login,
            'password' => $password    
          ]) ? static::findOne(['login' => $login, 'password' => $password ]) : null;

    }
    
    public static function findIdentity($id) {
        return static::findOne($id);      
   }

    public static function findIdentityByAccessToken($token, $type = null) {
         throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByUsername($login)   {
        return static::findOne([
            'login' => $login, 
        ]);
    }

    public function getId()  {
        return $this->id;
    }

    public function getAuthKey()   {
        return $this->authKey;
    }

    public function validateAuthKey($authKey) {
        return $this->authKey === $authKey;
    }  
}