<?php
namespace app\logics;

use app\models\User;
use yii\web;
use Yii;
use yii\db\Exception;

class UserManager
{
    public function Registration($login,$password) {
            $user = new User();
            $user->login = $login;
            $user->password = $this->GetHash($password);
            $user->authKey = $this->GenerateAuthKey();
            $user->date = strftime('%Y-%m-%d');
            if($user->save()){ return true;}
            else{return false;}   
    }
    
    public function GetHash($password){
        $hash = md5($password."callboard");
        return $hash; 
    }
    
    public function Login($user)   {
            $identity = $this->GetUser($user);
            if($identity){
                try{ return Yii::$app->user->login($identity, $user->rememberMe ? 3600*24*30 : 0);}
                catch(Exception $ex){}  
            }
            else{return false;}
    }       
    
    public function GetUser($user)   {
        if ($user->identity === false) {
            try{
            $user->identity = UserStore::findByloginAndPassword($user->login, $this->GetHash($user->password));
            }
            catch(Exception $ex){}
        }

        return $user->identity;
    }
    
    public function GenerateAuthKey()  {
        return Yii::$app->security->generateRandomString();
    }
   
    public function GetLoginById($id){
       $user = User::find()->where(["id"=>$id])->one();
       return $user->login;
    }
}

