<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\logics\UserManager;
use app\models\RegistrationForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\db\Exception;
use app\models\Profile;

class AuthenticationController extends Controller{
    private $loginForm;
    private $registrationForm;
    
    function __construct($id, $module){
        parent::__construct($id, $module);
        $this->loginForm = new LoginForm();
        $this->registrationForm = new RegistrationForm();
     }
     
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }
     
    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        if (Yii::$app->request->isAjax && $this->loginForm->load(Yii::$app->request->post())) {
              if($this->loginForm->validate()){
                $manager = new UserManager();
                if($manager->Login($this->loginForm)){ $this->goHome();}
                else{  
                    $this->loginForm->addError("login","User with such password and username does not exist.");
                    return $this->render('//site/login',["model"=>$this->loginForm]);  
                 }
              }
              else{ return $this->render('//site/login',["model"=>$this->loginForm]); }
        } 
        else{ $this->render('//site/login',["model"=>$this->loginForm]); }
    }
 
    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }
    
    public function actionRegistration(){
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        if (Yii::$app->request->isAjax && $this->registrationForm->load(Yii::$app->request->post())) {
          if($this->registrationForm->validate()){
                $manager = new UserManager();
                try{
                    $manager->Registration($this->registrationForm->login, $this->registrationForm->password);
                    $this->loginForm->login = $this->registrationForm->login;
                    $this->loginForm->password = $this->registrationForm->password;
                    $this->loginForm->rememberMe = false;
                    $manager->Login($this->loginForm);
                    $profile = new Profile();
                    $profile->user_id = Yii::$app->user->id;
                    $profile->save();
                    return $this->goHome();
                }
                catch(Exception $ex){
                    $this->registrationForm->addError("login","A user with this login already exists. Try another.");
                    return $this->render('//site/registration',['model'=>$this->registrationForm]);
                }   
            }
            else{ return $this->render('//site/registration',['model'=>$this->registrationForm]); }   
        }
        else{ return $this->render('//site/registration',['model'=>$this->registrationForm]); }
        
    }
}
