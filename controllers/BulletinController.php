<?php
namespace app\controllers;

use yii\web\Controller;
use app\models\AddBulletinForm;
use app\models\Bulletin;
use Yii;
use yii\web\UploadedFile;

class BulletinController extends Controller{
    
    private $addBulletinForm;

    function __construct($id, $module){
        parent::__construct($id, $module);
        $this->addBulletinForm = new AddBulletinForm();
     }
     
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }
    
    public function actionIndex(){
         return $this->render('addBulletin',['model' => $this->addBulletinForm] );
    }

    public function actionAddbulletin() {
        if(Yii::$app->request->post()){
              if($this->addBulletinForm->load(Yii::$app->request->post()) && $this->addBulletinForm->validate()){ 
                    $bulletin = new Bulletin();
                    $bulletin->user_id = Yii::$app->user->id;
                    $bulletin->title = $this->addBulletinForm->title;
                    $bulletin->date = strftime('%Y-%m-%d %H:%M:%S',$_SERVER['REQUEST_TIME']);
                    $bulletin->body = $this->addBulletinForm->body;
                    $bulletin->price = $this->addBulletinForm->price;
                    $file = UploadedFile::getInstance($this->addBulletinForm,'title_image');
                    if($file){
                        $imagePath = $file->baseName . '.' . $file->extension;
                        $file->saveAs('uploads/' . $imagePath);
                        $bulletin->title_image = $imagePath;  
                    }
                    try{
                      $bulletin->save();
                      $this->goHome();  
                    }  catch (Exception $ex){
                        $this->render('addBulletin',['model' => $this->addBulletinForm] );
                    }
                  }
                  else{return $this->render('addBulletin',['model' => $this->addBulletinForm]);}
              }
              else{
                  return $this->render('addBulletin',['model' => $this->addBulletinForm] );
              }  
      }
      
    public function actionSelect($id){
        $bulletin = Bulletin::find()
                ->where(["id"=>$id])->one();
        return $this->render('showBulletin',['bulletin' => $bulletin]);
    }
}
