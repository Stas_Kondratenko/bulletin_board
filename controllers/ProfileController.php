<?php
namespace app\controllers;

use yii\web\Controller;
use app\models\EditProfileForm;
use app\models\Profile;
use Yii;
use yii\web\UploadedFile;
use app\models\AddComentForm;
use app\models\Comment;
use app\models\AddRatingForm;
use app\models\Rating;
use yii\helpers\Url;

class ProfileController extends Controller{
    
    private $profileForm;
    private $profile;
    private $addCommentForm;
    private $addRatingForm;
            
    function __construct($id, $module){
        parent::__construct($id, $module);
        $this->profileForm = new EditProfileForm();
        $this->addCommentForm = new AddComentForm();
        $this->addRatingForm = new AddRatingForm();
     }
     
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }
    
    public function actionIndex(){
        $profile = Profile::find()->where(['user_id'=>Yii::$app->user->id])->one();
        if($profile){
            $this->profileForm->name = $profile->name;
            $this->profileForm->email = $profile->email;
            $this->profileForm->phone = $profile->phone;
            $this->profileForm->skype = $profile->skype;
            $this->profileForm->location = $profile->location;
            $this->profileForm->photo = $profile->photo;
        }
        return $this->render('editProfile', ['model' => $this->profileForm]);  
    }

    public function actionEditprofile(){
        $profile = Profile::find()->where(['user_id'=>Yii::$app->user->id])->one();
        if($profile){
            $this->profileForm->name = $profile->name;
            $this->profileForm->email = $profile->email;
            $this->profileForm->phone = $profile->phone;
            $this->profileForm->skype = $profile->skype;
            $this->profileForm->location = $profile->location;
            $this->profileForm->photo = $profile->photo;
        }
            if(Yii::$app->request->post()){
                    if($this->profileForm->load(Yii::$app->request->post()) && $this->profileForm->validate()){  
                       $profile = Profile::find()->where(['user_id' => Yii::$app->user->id])->one();
                       $profile->user_id = Yii::$app->user->id;
                       $profile->name = $this->profileForm->name;
                       $profile->email = $this->profileForm->email;
                       $profile->phone = $this->profileForm->phone;
                       $profile->skype = $this->profileForm->skype;
                       $profile->location = $this->profileForm->location;
                       $image = UploadedFile::getInstance($this->profileForm,'photo');
                            if($image){
                                $profile->photo = base64_encode(file_get_contents($image->tempName));
                            }
                           try{$profile->save();}
                           catch (Exception $ex){return $this->render('editProfile',['model' => $this->profileForm] ); }
                                 return $this->refresh(); 
                            }  
                             else{
                                 return $this->render('editProfile',['model' => $this->profileForm] ); 
                             } 
               }
        else{
              return $this->render('editProfile', ['model' => $this->profileForm]);  
       }
    }
    
    public function actionSelect($id){
       $this->profile =  Profile::find()
                  ->where(["profile.user_id"=>$id]) 
                  ->with('bulletins')
                  ->with('comments')
                  ->one();
       $this->profile->comments;
       return $this->render('showProfile',['profile' => $this->profile,
           "commentModel"=>$this->addCommentForm,
           "ratingModel"=>$this->addRatingForm] ); 
    }
   
    public function actionAddcomment(){
          $coment = new Comment();
          if($this->addCommentForm->load(Yii::$app->request->post()) && $this->addCommentForm->validate()){
           $coment->body = $this->addCommentForm->body;
           $coment->date = strftime('%Y-%m-%d %H:%M:%S',$_SERVER['REQUEST_TIME']);
           $coment->author_id = Yii::$app->user->id;
           $coment->user_id = $this->addCommentForm->user_id;
           try{
               $coment->save();
               return $this->redirect(Url::to(['select', 'id' => $this->addCommentForm->user_id]));
           }
           catch (Exception $ex){ 
              return $this->refresh();
           }
        } 
        else{ return $this->goBack();}   
      }  
      
    public function actionAddrating(){
          $rating = new Rating();
          if($this->addRatingForm->load(Yii::$app->request->post()) && $this->addRatingForm->validate()){
           $rating->rating = $this->addRatingForm->rating;
           $rating->author_id = Yii::$app->user->id;
           $rating->user_id = $this->addRatingForm->user_id;
           try{
               $rating->save();
               return $this->redirect(Url::to(['select', 'id' => $this->addRatingForm->user_id]));
           }
           catch (Exception $ex){ return $this->refresh();}
        } 
        else{ return $this->goBack();}     
      }
}
