<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\LoginForm;
use app\models\RegistrationForm;
use app\models\Bulletin;
use yii\data\Pagination;

class SiteController extends Controller{
 
    private $bulletins;
    private $bulletinsPages;

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()  {
        $this->bulletins = Bulletin::find();
        $countQuery = clone $this->bulletins;
        $this->bulletinsPages = new Pagination(['totalCount' => $countQuery->count(),'pageSize'=>20]);
        $this->bulletins= $this->bulletins->offset($this->bulletinsPages->offset)
                 ->limit($this->bulletinsPages->limit)
                 ->orderBy([
                    'date' => SORT_DESC
                  ])
                 ->all();
        return $this->render('index',['bulletins'=>$this->bulletins,"bulletinsPages"=>$this->bulletinsPages]);
    }

    public function actionLogin(){
        return $this->render('login',['model'=>new LoginForm()]);
     }
     
    public function actionRegistration(){
        return $this->render('registration',['model'=> new RegistrationForm()]);
     }  
}
