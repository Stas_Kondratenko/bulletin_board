<?php
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
$this->title = 'Add new advert';
?>

<section class="container">
        <section class="col-md-offset-3 col-md-6 center-form">
         <?php Pjax::begin(['timeout' => 5000]); ?>
            <?php $form = ActiveForm::begin([
                             'id' => 'add-bulletin-form',
                             'method' => 'post',
                             'action' => ['bulletin/addbulletin'],
                             'options' => ['enctype'=>'multipart/form-data',
                                 'onsubmit' => 'return false',
                                 'data-pjax' => true],
                             'enableClientValidation' => true,
                              ]); ?>
                  <div class="form-group">
                    <label class="control-label col-xs-2">Title</label>
                    <div class="col-xs-10">
                       <?= $form->field($model, 'title')->label(false) ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-xs-2">Description</label>
                    <div class="col-xs-10">
                       <?= $form->field($model, 'body')->textArea(['rows' => '6'])->label(false) ?>
                    </div>
                  </div>
                  <div class="form-group">
                        <label class="control-label col-xs-2">Price in $</label>
                        <div class="col-xs-10">
                            <?= $form->field($model, 'price')->label(false) ?>
                        </div>
                    </div>
                  <div class="form-group">
                    <label class="control-label col-xs-4">Title image</label>
                    <div class="col-xs-8">
                       <?= $form->field($model, 'title_image')->fileInput(['accept' => 'image/gif,image/jpeg,image/png'])->label(false) ?>
                    </div>
                  </div>
                
                  <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" id="login-btn">Add</button>
                      <button type="reset" class="btn btn-default" data-dismiss="modal">Reset</button>
                  </div>

                 <?php ActiveForm::end(); ?>
            <?php Pjax::end(); ?>
        </section>
    </section>