<?php
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;
use app\logics\UserManager;
$manager = new UserManager();
$authorName = $manager->GetLoginById($bulletin->user_id);
$this->title = 'Bulletin '.$bulletin->id;
?>
    <div class="single-product-area">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <div class="product-content-right">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="product-images">
                                    <div class="product-main-img">
                                        <?php echo Html::img('@web/uploads/'.$bulletin->title_image) ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6">
                                <div class="product-inner">
                                    <h2 class="product-name"><?php echo $bulletin->title ?></h2>
                                    <div class="product-inner-price">
                                        <ins>Price: $<?php echo $bulletin->price ?></ins> 
                                    </div>    
                                    
                                    <form action="" class="cart">
                                        <h4>Author: <?php echo $authorName; ?></h4>
                                        <?php echo Html::a("Connect with ".$authorName,['profile/select', 'id' =>$bulletin->user_id ] ,['data-pjax'=>0,"class"=>"add_to_cart_button"]) ?>
                                    </form>   
                                    
                                    <div class="product-inner-category">
                                        <h5>Date of adverting: <?php echo $bulletin->date ?></h5>
                                    </div> 
                                    <div role="tabpanel">
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="home">
                                                <h2>Product Description</h2>  
                                                <p><?php echo $bulletin->body ?></p> 
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>