<?php
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>
<section class="container">
     <h2>Add you'r vote</h2>
     <h4>Remember, you can vote only once!</h4>
    <div class="submit-review col-md-5">
          <?php Pjax::begin(['timeout' => 5000]); ?>
            <?php $form = ActiveForm::begin([
                             'id' => 'add-rating-form',
                             'method' => 'post',
                             'action' => ['profile/addrating'],
                             'options' => [
                                 'onsubmit' => 'return false',
                                 'data-pjax' => true],
                                 'enableClientValidation' => true,
                              ]); ?>

                    <div class="rating-chooser">
                        <p>Your rating</p>
                         <?= $form->field($model,'rating')->dropDownList(["1"=>"1","2"=>"2","3"=>"3",'4'=>"4","5"=>"5"])->label(false) ?>
                        
                         <?= $form->field($model, 'user_id')->hiddenInput(['value'=>$_GET['id']])->label(false); ?>

                    </div>

                 <div class="modal-footer">
                       <button type="submit" class="btn btn-primary" id="login-btn">Submit</button>
                </div>

                 <?php ActiveForm::end(); ?>
            <?php Pjax::end(); ?>
    </div>
</section>