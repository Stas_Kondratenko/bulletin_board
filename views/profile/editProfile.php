<?php
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;
$this->title = 'My profile';
?>
<section class="container">
    <div class="col-md-offset-2 col-md-4" style="margin-top:3%; margin-bottom: 3%">
        <h5>My photo</h5>
         <?php
                if($model->photo){
                    echo Html::img('data:image/jpg;base64,'.$model->photo);
                }
            ?>
    </div>
        <section class="col-md-4 center-form">
            
         <?php Pjax::begin(['timeout' => 5000]); ?>
            <?php $form = ActiveForm::begin([
                             'id' => 'editProfile-form',
                             'method' => 'post',
                             'action' => ['profile/editprofile'],
                             'options' => ['enctype'=>'multipart/form-data',
                                 'onsubmit' => 'return false',
                                 'data-pjax' => true],
                             'enableClientValidation' => true,
                              ]); ?> 
                  <div class="form-group">
                    <label class="control-label col-xs-3">Name</label>
                    <div class="col-xs-9">
                       <?= $form->field($model, 'name')->label(false) ?>
                    </div>
                  </div>
                  <div class="form-group">
                        <label class="control-label col-xs-3">E-mail</label>
                        <div class="col-xs-9">
                            <?= $form->field($model, 'email')->label(false) ?>
                        </div>
                    </div>
                    <div class="form-group">
                                <label class="control-label col-xs-3">Phone</label>
                                <div class="col-xs-9">
                                    <?= $form->field($model, 'phone')->label(false) ?>
                                </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-xs-3">Skype</label>
                        <div class="col-xs-9">
                            <?= $form->field($model, 'skype')->label(false) ?>
                        </div>

                    </div>
                     <div class="form-group">
                        <label class="control-label col-xs-3">Location</label>
                        <div class="col-xs-9">
                            <?= $form->field($model, 'location')->label(false) ?>
                        </div>

                    </div>
                  <div class="form-group">
                    <label class="control-label col-xs-4">Photo</label>
                    <div class="col-xs-8">
                       <?= $form->field($model, 'photo')->fileInput(['accept' => 'image/gif,image/jpeg,image/png'])->label(false) ?>
                    </div>
                  </div>
                
                 <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" id="login-btn">Save</button>
                      <button type="reset" class="btn btn-default" data-dismiss="modal">Clear</button>
                  </div>   
                 <?php ActiveForm::end(); ?>
              <?php Pjax::end(); ?>
        </section>
    </section>