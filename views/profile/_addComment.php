<?php
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>
<section class="container">
     <h2>Add comment</h2>
    <div class="submit-review col-md-5">
          <?php Pjax::begin(['timeout' => 5000]); ?>
            <?php $form = ActiveForm::begin([
                             'id' => 'add-comment-form',
                             'method' => 'post',
                             'action' => ['profile/addcomment'],
                             'options' => [
                                 'onsubmit' => 'return false',
                                 'data-pjax' => true],
                                 'enableClientValidation' => true,
                              ]); ?>

                  <div class="form-group">
                       <?= $form->field($model, 'body')->textArea(['rows' => '10'])->label("Your comment")?>
                  </div>
       
                     <?= $form->field($model, 'user_id')->hiddenInput(['value'=>$_GET['id']])->label(false); ?>

                <div class="modal-footer">
                       <button type="submit" class="btn btn-primary" id="login-btn">Submit</button>
                </div>

                 <?php ActiveForm::end(); ?>
            <?php Pjax::end(); ?>
    </div>
</section>