<?php
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;
use app\logics\UserManager;
use app\models\Rating;
$manager = new UserManager();
$authorName = $manager->GetLoginById($profile->user_id);
$this->title = $authorName."'s profile";
?>
    <div class="single-product-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-content-right">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="product-images">
                                    <div class="product-main-img">
                                        <?php 
                                        if($profile->photo){
                                             echo Html::img('data:image/jpg;base64,'.$profile->photo);
                                        }else{
                                            echo Html::img('@web/img/anonymous.png');
                                        }
                                        ?>
                                    </div>
                                </div>
                                
                            <div class="related-products-wrapper" style="margin-top: 10% !important">
                                <h2 class="related-products-title"><?php echo $authorName."'s bulletins"; ?></h2>
                            <div class="related-products-carousel">
                                
                         <?php
                           foreach ($profile->bulletins as $bulletin){
                               echo "<div class='single-product'>
                                        <div class='product-f-image'>".
                                            Html::img('@web/uploads/'.$bulletin->title_image)."
                                            <div class='product-hover'>".
                                                 Html::a("See details",['bulletin/select', 'id' =>$bulletin->id ] ,['data-pjax'=>0,"class"=>"view-details-link"])." 
                                             </div>
                                        </div>
                                     <h2>$bulletin->title</h2>
                                     <div class='product-carousel-price'>
                                         <ins>$$bulletin->price</ins>
                                     </div> 
                                   </div>";
                               }
                                ?>
                            </div>
                          </div>
                     </div>
                            
                            <div class="col-sm-5">
                                <div class="product-inner">
                                    <h2 class="product-name"><?php echo $profile->name ?></h2>
                                     <div class="product-inner-price">
                                        <ins>Rating:</ins> 
                                        <h4><?php echo Rating::find()->where(["user_id"=>$profile->user_id])->average("rating"); ?></h4>
                                    </div>   
                                    <div class="product-inner-price">
                                        <ins>E-mail:</ins> 
                                        <h4><?php echo $profile->email ?></h4>
                                    </div>   
                                    <div class="product-inner-price">
                                        <ins>Phone:</ins> 
                                        <h4><?php echo $profile->phone ?></h4>
                                    </div>  
                                    <div class="product-inner-price">
                                        <ins>Skype:</ins> 
                                        <h4><?php echo $profile->skype ?></h4>
                                    </div>  
                                    <div class="product-inner-price">
                                        <ins>Location:</ins> 
                                         <h4><?php echo $profile->location ?> </h4>
                                    </div>  
                                    
                                       <div role="tabpanel">
                                        <ul class="product-tab" role="tablist">
                                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Comments</a></li>
                                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Leave a comment</a></li>
                                            <li role="presentation"><a href="#rating" aria-controls="profile" role="tab" data-toggle="tab">Rating</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="comment tab-pane fade in active" id="home">   
                                           <?php 
                                           if($profile->comments){
                                             foreach ($profile->comments as $comment){
                                               echo" <div class='media'>
                                                        <div class='media-body'>
                                                            <h5 class='media-heading'>".
                                                                 Html::a( $manager->GetLoginById($comment->author_id),['profile/select', 'id' =>$comment->author_id ] ,['data-pjax'=>0])
                                                               .
                                                                "<small>   ".$comment->date."</small>
                                                            </h5>
                                                            ".$comment->body."
                                                        </div>
                                                    </div>";
                                             }
                                           }else{
                                                echo '<h4>There are not comments. Bee first!</h4>';
                                           }
                                                ?>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="profile">
                                                <?php
                                                if(!Yii::$app->user->isGuest){
                                                    if($profile->user_id == Yii::$app->user->id){
                                                        echo "<h4>You can't add comments to yourself!</h4>";
                                                    }else{
                                                         echo $this->render("_addComment",["model"=>$commentModel]); 
                                                    }
                                                }else{
                                                    echo '<h4>Only registered users can add comments!</h4>';
                                                }
                                                ?>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="rating">
                                                <?php
                                                
                                                $rating = Rating::find()->where(["user_id"=>$profile->user_id,"author_id"=>Yii::$app->user->id])->one();
                                                if(!Yii::$app->user->isGuest){
                                                    if($profile->user_id == Yii::$app->user->id){
                                                        echo "<h4>You can't to vote to yourself!</h4>";
                                                    }else{
                                                        if(!$rating){
                                                           echo $this->render("_addRating",["model"=>$ratingModel]);  
                                                        }  else {
                                                            echo "<h4>You vote $rating->rating.</h4>";
                                                            echo "<h4>You can to vote only once!</h4>";
                                                        }
                                                    }
                                                }else{
                                                    echo '<h4>Only registered users can to vote!</h4>';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                     </div>   
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>