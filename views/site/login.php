<?php
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
$this->title = 'Log in';
?>
<div class="container">
    <div class="col-md-offset-4 col-md-4 center-form">
       
              <?php Pjax::begin(['timeout' => 5000]); ?>
                 <?php $form = ActiveForm::begin([
                             'id' => 'login-form',
                             'method' => 'post',
                             'action' => ['authentication/login'],
                             'options' => ['onsubmit' => 'return false','data-pjax' => true],
                             'enableClientValidation' => true,
                              ]); ?>
            
                  <div class="form-group">
                       <?= $form->field($model, 'login')->label("Login") ?>
                  </div>
                  <div class="form-group">
                       <?= $form->field($model, 'password')->passwordInput()->label("Password") ?>
                  </div> 
                  <div class="form-group">
                       <?= $form->field($model, 'rememberMe')->checkbox() ?>
                  </div>
                  <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" id="login-btn">LogIn</button>
                      <button type="reset" class="btn btn-default" data-dismiss="modal">Clear</button>
                  </div>
                 <?php ActiveForm::end(); ?>
              <?php Pjax::end(); ?>
   
    </div>
</div>
            
           