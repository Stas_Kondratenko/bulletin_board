<?php
use yii\widgets\LinkPager;
use yii\helpers\Html;
$this->title = 'Bulletin board';
?>
    <div class="single-product-area">
        <div class="container">
            <?php 
            foreach ($bulletins as $bulletin){  
               echo"   <div class='col-md-3 col-sm-6'>
                    <div class='single-shop-product'>
                        <div class='product-upper'>".
                            Html::img('@web/uploads/'.$bulletin->title_image).
                        "</div>
                        <h2>$bulletin->title</h2>
                        <div class='product-carousel-price'>
                            <ins>$$bulletin->price</ins>
                        </div>  
                        <div class='product-option-shop'>".
                           Html::a("More...",['bulletin/select', 'id' =>$bulletin->id ] ,['data-pjax'=>0,"class"=>"add_to_cart_button"])."
                        </div>                       
                    </div>
                </div> ";    
            }
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="product-pagination text-center">
                        <nav>
                            <?php  
                           echo LinkPager::widget([
                                'pagination' => $bulletinsPages,
                                'options'=>["class"=>"pagination"]
                           ]); 
                         ?>        
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

